## Restart codeigniter ##
* ./bin/codeigniter4_deploy_script.sh

## Connect to morbegno74 server ##
* ssh dev-bigcommerce-ips-it@websrv-morbegno74.iplusservice.it

## Rsync to webroot ##
1. App Folder-> rsync --delete -avi ~/gitlab.iplusservice.it/nexi/bigcommerce/bigcommerce-app/app/ dev-bigcommerce-ips-it@websrv-morbegno74.iplusservice.it:documentroot/app/
2. Public Folder-> rsync --delete -avi ~/gitlab.iplusservice.it/nexi/bigcommerce/bigcommerce-app/public/ dev-bigcommerce-ips-it@websrv-morbegno74.iplusservice.it:documentroot/public/

## Create an alias ##
* alias {chosenName}='{command}'

## Restart db environment
* cat environment/oracle-startup/100_PIS.sql | docker container exec -i environment_oracle-db_1 sqlplus system/root@localhost/xe
